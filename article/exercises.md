##### Exercise 1

1. This is a pen. That is a pencil. +
2. This is a book. It is my book. +
3. Is this your pencil? -- No, it isn't my pencil, it's my sister's pencil. +
4. I have a sister. My sister is an engineer. My sister's husband is a doctor. They have got two children. +
5. This is a girl. This girl is their daughter. Their daughter's name is Vera. +
6. That is a boy. That boy is her brother. Her brother's name is Nick. +
7. This is our flat. +
8. We have got a car. Our car is not expensive, but reliable. +
9. I have no pet. +
9. My granny has got a headache. She has no idea what to do. -
10. I have a friend. His name is Mike. My friend is very good. +
11. It's a cat. Its tail is long and bushy. It's Mike's cat. +

---

##### Exercise 2

1. When is your birthday? -- My birthday is on the 1st May. -
2. Do you remember your mother's birthday? -- Yes, I do. +
3. His uncle is generous and her aunt is very kind. +
4. That man is very clever. His book is recognized by a lot of people. +
5. They know our address. +
6. Their son speaks English very well. +
7. My cousin's dog is small. Its hair is curly. +
8. Is this a watch? -- No, it isn't a watch, it's a pen. -
9. This pen is good, and that pen is bad. +
10. I can see a pencil on your, but I can see no paper. -
11. Give me a chair, please. +
12. They have have a dog and two cats. -
13. I have a spoon in my soup plate, but I have no soup in it. -
14. My friend says he is going to be a millionaire one day. +
15. Would you like an orange? -
16. Mr Smith is an artist. Mrs Smith is a poetess. She is not a singer. -

---

##### Exercise 3

1. He hasn't got a car. But he's got a computer. The computer is new. +
2. His friend have got a cat and a dog. The dog never bites the cat. +
3. This is a tree. The tree is green. +
4. I can see three boys. The boys are playing. +
5. I have a bicycle. The bicycle is black. My friend has no bicycle. +
6. Our room is large. +
7. We wrote a dictation yesterday. The dictation was long. +
8. She has two daughters and one son. Her son is a pupil. +
9. Last year I gave my mother a bracelet for her birthday. She liked the bracelet. +
10. My brother's friend has no dog. +
11. This pencil is broken, Give me that pencil, please. +
12. She has a ball. The ball is big. +
13. I got a letter from my friend yesterday. The letter was interesting. +
14. When they were in Geneva, they stayed at hotel. Sometimes they had dinner at the hotel and sometimes in a restaurant. +
15. I've got an idea. +
16. What a surprise! Our parents gave us a DVD player for Christmas. +

---

##### Exercise 4

1. This is a pen. The pen is red. +
2. These are pencils. The pencils are black. +
3. This is soup. The soup is so tasty. -
4. In the morning I eat a sandwich and drink tea. +
5. She gave me coffee and a cake. The coffee was hot. The cake was very tasty. -
6. Do you like ice cream? -
7. I see a book in your hands. Is the book interesting? +
8. Do you need a camera? +
9. He never eats meat, he always eats vegetables, cereals, seeds, fruit, and nuts. He is a vegetarian. +
10. This is a pineapple. The pineapple is delicious. +
11. Elaine, apples are good for you! +
12. My cousin is upset. He's got a sore throat. +
13. This is cottage cheese. The cottage cheese is fresh. -
14. She bought meat, butter and potatoes yesterday. She also bought a cake. The cake was delicious. We ate the cake and drank tea -
15. This is my table. On the table I have books, two pencils, a pen and paper. +
16. This is a bag. The bag is brown. It is my sister's bag. And this is my bag. It is yellow. +

---

##### Exercise 5

1. I have two sisters. My sisters are students. +
2. We are at home. +
3. My brother is not at home, he is at school. +
4. My mother is at work. She is a doctor. +
5. I an not a doctor. +
6. He has no sister. +
7. He is not a pilot. +
8. I have thirty-two teeth. +
9. He has a child. +
10. She has two children. Her children are at school. +
11. Is your father at home? -- No, he is at work. +
12. Where is your brother? -- He is at home. +

---

##### Exercise 6

1. We have a large family. +
2. My granny often tells us long interesting stories. +
3. My father is an engineer. He work in a factory. The factory is large. +
4. My mother is a doctor. She works at a large hospital. She is at work now. -
5. My aunt is a teacher. She works at school. The school is good. My aunt isn't at school now. She is at home. She is drinking tea and eating jam. The jam is sweet. I am at home, too. I am drinking tea and eating a sandwich. The sandwich is tasty. +
6. My sister is at school. She is a pupil. +
7. My cousin has a big black cat. My cousin's cat has two kittens. The cat likes milk. The kittens like milk, too. -
8. I am an engineer. +
9. My son is a pupil. +
10. He is a good pupil. +
11. This is a house. +
12. This is my laptop. +
13. You have some pencils, but I have no pencil. Give me a pencil, please. +
14. It's a small animal that has long ears, a short tail, and soft hair. -

---

##### Exercise 7

1. What's the weather like today? -- The weather is fine. +
2. The sun is yellow. +
3. The sky is grey today. +
4. The Earth is a planet. +
5. We had an English lesson yesterday. The teacher asked me many questions. The questions were very difficult. -
6. Where is your brother? -- He is at home. He is in his room. He is sitting at the table. He is doing his homework. The homework is difficult. +
7. Our cat is sitting on the sofa. +
8. It is very dark in the room. Turn on the light, please. +
9. Nick went into the bathroom, turned on the water and washed his hands.
10. Ann turned on the television to watch the evening news.
11. She doesn't often watch television. -
12. You can't see the moon in the sky tonight. +

---

##### Exercise 8

1. This is a good book. Take the book from the table. Put this book in the bookcase. -
2. The weather is fine today. The sky is blue. The sun is shining brightly in the blue sky. +
3. This is a boy. The boy is at school. He is a pupil. This boy is my brother's friend. He has a cat, but he has no dog. He likes his cat. He gives the cat milk every day. -
4. Yesterday I received a letter from my friend. The letter was important. +
5. We live in a big house. I like the house very much. +
6. Are you a worker? -- No, I am a student. +
7. I like your beautiful flower. Give me the flower, please. +
8. My mother is at home. She is reading a marvellous story. +
9. My father is not at home. He is at work. He is not a lawyer. He is a doctor. He is a good doctor. He works at a hospital. The hospital is large. -
10. That is a book. The book is thick. That book isn't thin. This is an interesting thick book. +
11. Those are books. The books are new and old. Those are new and old books. Those books are interesting. -

---

##### Exercise 9

1. There is a big tree in the garden. +
2. There is a bank near here -- Where is the bank? +
3. There is a new supermarket in the centre of our town. +
4. There is a hotel over there. The hotel isn't cheap. +
5. Where is the cat? -- The cat is on the sofa. +
6. Where is the book? -- The book is on the shelf. +
7. Where are the flowers? -- The flowers are in a beautiful vase. +
8. Where is the vase? -- The vase is on a little table near the window. +
9. Open the window, please. The weather is fine today. I can see the sun in the sky. I can see a nice little bird. The bird is sitting in a big tree. The tree is green. +
10. There is a little white cloud in the sky. +
11. What a beautiful day! -
12. We have a large room. There is a big sofa in the room and a little lamp on the wall over the sofa. I like to sit on the sofa and read a good book. -
13. This is a computer. The computer isn't old. This computer is new. This is a good new computer.
14. These are computers. The computers are new. These aren't old computers. These are new computers. These computers are good. +

---

##### Exercise 10

1. There is a wonderful small computer in front of the books there. -
2. Where is the soup? -- The soup is in a big saucepan on the gas cooker. +
3. Where are the cutlets? -- The cutlets are in the refrigerator on a little plate. +
4. There is no bread on the table. Where is the bread? +
5. There is a little brown coffee table in our room in front of the sofa. -
6. Where is the table in your room? +
7. There is a thick carpet on the floor in my mother's room. +
8. Is your brother at home? -- No, he is at work. He works in a big factory. He is an engineer. -
9. My sister has many books. The books are in a big bookcase. She has really good taste in books. -
10. The weather is fine today. Let's go and play in the yard. There are many children in the yard. They are playing with a ball. +

---

##### Exercise 11

1. I see a bottle of pineapple juice on the kitchen table. +
2. Her son has a great sense of humor. +
3. There was a disco at the club last Sunday but he didn't go. +
4. Is there a bus stop near the building? +
5. We have a big dog. The dog is very clever. +
6. My friend has a very good computer. +
7. This boy is big. He is a student. +
8. There is a large piano in the hall. +
9. This is the tree and that is not a tree. It's a bush. -
10. I am a boy. I am a pupil. I study at school. -
11. My sister is at work. She is a secretary. She works for a new company. +
12. This is a very difficult question. I don't know the answer to it. -
13. Do you see a little girl with a big ball in her hands? She is a pupil of our school. -
14. There was a beautiful flower in this vase yesterday. Where is the flower now? +
15. Last year we were in Geneva. It is an exciting city to visit, but a very expensive place to live. -

---

##### Exercise 12
1. There is a jar of delicious orange marmelade in the middle of the shelf. +
2. There is a big box of cereal to the right of you. +
3. There is a bunch of bananas on the table. Don't keep them in the fridge. -
4. There is a loaf of white bread on the upper shelf of the fridge. If you want your bread to be fresh, keep it only in the fridge. -
5. Is there a bag of flour in the cupboard? +
6. There was a bottle of drinking water in the corner of the kitchen. +
7. There is a thick red carpet in my room. The carpet is on the floor on the front of the sofa. -
8. Where is the table in your brother's room? -- His table is near the window. +
9. I can see a fine vase on the shelf. There are lovely flowers in the vase. +
10. They have no piano in their living room. +
11. My uncle is married. He has a beautiful wife. They have a son, but they have no daughter. +
12. I can see a nice coffee table in the middle of the room to the right of the door. +
13. Our TV set is on a little table in the corner of the room. -
14. There is a beautiful picture in my father's study, The picture is on the wall to the left of the window. +
15. What a picture! +
---

##### Exercise 13

My aunt's flat is in a new house. -
There is a living room, a bedroom, a study, a bathroom, and a kitchen in the flat. -
The bedroom is a large room with two windows. +
The room is light as windows are large. +
There are white curtains on the windows. +
There are two beds with large pillows on them. +
There are small tables near the beds. +
There are lamps on them. +
To the left of the door there is a dressing table with a mirror on it. +
There is a low chair at the dressing table. +
There are several pictures on the pale green walls. -
There is a thick carpet on the floor. +
The carpet is dark green. +
The room is very cosy. +

---

##### Exercise 14
1. There is a park behind the hostpital. There are beautiful trees in the park. +
2. There is a good film on TV this evening. I am going to watch it. +
3. There is a library between the school and the bank. There are English and German books in this library. -
4. There is a sofa in the corner of the room. +
5. There are cushions on the sofa. +
6. There are books on the shelf. Give me a book, please. +
7. What can you see in the fridge? -- There is sausage on the top shelf, but there is no cheese there. There is butter in the butter dish. There are tomatoes and carrots on the bottom shelf. There are eggs and apples on the next shelf. There is an orange, a lemon, and jam in a little jar there. -
8. There is juice in this carton. May I drink the juice? -
9. There are girls in the yard, but I can see no boys. There are the boys? -- Oh, all the boys are playing football at the stadium. -
10. There is a peculiar charm in her voice. +
11. There is money in the purse. +
---

##### Exercise 15
1. Where is the bus station? -- The bus station is next to the gas station. +
2. There are two pets in the house: a cat and a dog. +
3. There is a TV antenna on the roof. +
4. There is a mailbox between the building and the bus stop. +
5. There is a big dog in the front of the fireplace. -
6. Do you speak English at work? +
7. She had a bad day today. +
8. I have a colour TV set. The TV set is on a little table in the corner of the room. -
9. There is a book, a pen, and paper on my writing desk. -
10. My brother is a teacher. He works at a school. He has very good books. His books are in a big bookcase. -
11. There is tea in my glass. There is no tea in my friend's glass. His glass is empty +
12. Where is the coffee table in your room? -- The coffee table is in front of the sofa. There is a cup on the coffee table and a box of chocolates. There is coffee in the cup.
13. There are photographs and newspapers on the sofa. +
14. There is a guitar on the chair near the piano. +
15. There was a piano in the corner of the living room. He sat at the piano for hours, playing favourite pieces from classical music. He was a wonderful piano player. +
---

##### Exercise 16
1. Every day my brother and I get up at eight o'clock and walk to school. I like school. It's fun. My brother loves football. He hates homework. So he doesn't like to go to school. Will he go to work in the future? -
2. My friend has to get up early in the morning because he goes to school. That's why he usually goes to bed early in the evening. +
3. The weather was very bad in the morning yesterday. The sky was grey and it was raining. But in the middle of the day the weather began to change. The rain stopped and the sun appeared from behind the clouds. In the afternoon it was very warm. I did not want to stay at home and went into the yard. There were boys and girls in the yard. We played in the yard till late in the evening. When I came home, I drank tea, ate a sandwich and went to bed at once. I slept very well at night. -
---

##### Exercise 17
1. My brother is a pupil. He goes to school. He goes to school in the morning. He has five or six lessons every day. In the afternoon he goes home. At home he does his homework. In the evening he reads books. He usually goes to bed at half past seven. At night he sleeps. +
2. My father goes to work in the morning and comes home in the evening. +
3. I get up at half past seven in the morning and go to bed at a quarter to eleven in the evening. -
4. When does your mother leave home for work? -- She leaves home for work at a quarter past eight. +
5. When do you leave home for school? -- I leave home for school at half past eight. -
6. What do you do when you come home from school? -- I do my homework, talk to my friends on the phone and go for walks. I often listen to music. I like jazz best. Sometimes I play computer games. -
---

##### Exercise 18
1. We always go to the Russian Museum on sunday. +
2. On Saturday she usually goes to the Philarmonic. +
3. In August he has his birthday. He is planning to have a nice party with his friends. +
4. There are three rooms and a kitchen in our new flat. +
5. My new dress is made of silk. +
6. If you want to write something on the blackboard, you must have a piece of chalk. +
7. Are there any pupils in the classroom? +
8. I have a new English book. I find the book quite fascinating. +
9. There is a garden in front of our school. The garden is not large, but it is very nice. +
10. May is the fifth month of the year. +
11. Saturday is the sixth day of the week. +
12. Sunday is a day off. +
13. Today is the ninth of May. Anton has got a new mobile phone. He is going to make a phone call to his grandfather. +
14. This is a nice place. In June we are going there for a holiday. It'll be great fun. -
---

##### Exercise 19
1. This is a classroom. The classroom is large and light. +
2. There is a picture on the wall. +
3. What is the date today? It is the seventh of December. -
4. The third lesson today is a lesson of English. -
5. Pete, go to the blackboard. +
6. After school I usually go home. +
7. My father always comes from work late: at eight o'clock or at half past eight. But on Friday he comes home early: at half past four or at a quarter to five. On Saturday and on Sunday he does not go to work. -
8. My friends live in a small town. It is a new town. The streets in the town are broad and straight. There are beautiful buildings in them. The town is very green, and so the air is fresh. There are beautiful parks and gardens in the town. People like to go there after work. In the evening you can hear sounds of music from the parks. There are schools, libraries, a supermarket, a hospital, a theatre, cinemas, clinics and kindergartens in the town. +
---

##### Exercise 20
I go to school in the morning, so I get up early. I usually get up at a quarter past seven. I go to the bathroom, turn on the water and wash my face and hands. My father and mother also get up early in the morning. My mother works in an office. She is a typist. My father is a doctor. He works at a hostpital. We have breakfast in the kitchen. We eat porridge and eggs. We drink tea. My father and mother leave home for work at half past eight. My father goes to the hospital and my mother goes to the office. I don't leave home with my parents: the school where I study is near our house. I leave home for school at a quarter to nine. My granny stays at home and cooks dinner. I have lunch at school after the third lesson. My father and mother have lunch at work. When we come home, we have dinner. -
---

##### Exercise 21
1. Every day my husband goes to work, my son goes to school and I go to the institute. +
2. There is a new school at the corner of our street. +
3. My daughter came home from school on Monday and said to me, "There will be a parents' meeting on the tenth of February at six o'clock in the evening." -
4. The teacher read us a very interesting story at the lesson. +
5. When the bell rang, the pupils went into the classroom. -
6. We are usually at school from nine o'clock in the morning till two o'clock in the afternoon. +
7. We don't go to school on Sunday. +
8. We stay at home and relax. I like to read books for relaxation. +
---

##### Exercise 22
1. What do you do after breakfast? -- After breakfast I go to school. +
2. My granny likes to read a book after lunch. +
3. People usually have breakfast in the morning. They have dinner in the afternoon. In the evening people have supper. +
4. There is a proverb: "After dinner sleep awhile, after supper walk a mile." +
5. Who cooks dinner in your family? +
6. Yesterday father told us a very amusing story at breakfast. +
7. What did you have for lunch at school on Wednesday? -- We had salad and tea. -
8. My mother never has supper with family because she does not like to eat in the evening. +
9. When do you clean your teeth in the morning: before breakfast or after breakfast? +
10. I make cakes for tea. +
---

##### Exercise 23
1. For breakfast I have coffee wit milk and sugar. I have jam, too. There are different things on the dinner table. There is bread, butter and ham. There are cups and glasses there. There is a jug on the table. The milk in the jug is hot. There is a teapot on the table. There is tea in the teapot. The tea is hot, too. There are plates, forks and knives on the table. -
2. The lamp is on the table. -
3. There is a lamp on the table. +
4. Is there a lamp on table? +
5. Is the lamp on the table? -
6. Is there a clock on the wall? +
7. There are two shelves on the wall? +
8. Where are the shelves? -- the shelves are on the wall. +
9. They have a large flat. There are four rooms in the flat. +
10. Is the newspaper on the table? -
11. Is there a newspaper on the table? +
12. There is butter on the plate. +
13. Where is the butter? -- the butter is on a little plate. +
14. There is white and brown bread on the table. -
15. What time do you usually have dinner? -- We eat dinner at five. +
16. We often have fish for dinner. -
---

##### Exercise 24
1. Pete has a small family. He has a father andba mother. He has no brother, but he has a sister. His sister is a pupil. She is a good girl. She has Russian books, but she has no English books. -
2. There is a writing desk in the room. The writing desk is good. There is a lamp on the writing desk. +
3. My uncle has a large family. They are six in the family. +
4. My father is an engineer. He works in a big factory. +
5. We have a good library. Our books are in a big bookcase. -
6. Is your sister married? +
7. What do you do after breakfast? -- I go to school. +
8. When do you come home? -- I come home at half past two. +
9. Do you like to watch TV in the evening? +
10. He watches television all day. I never do it. I can't watch TV very often. But tonight I can spend the evening in front of the box, because there is a very exciting film on. It is "My Fair Lady" with Rex Harrison and Audrey Hepburn. I love it. +
11. We bought a new telly last week. +
12. Is there anything good on telly tonight? -
---

##### Exercise 25
1. My aunt and my uncle are doctors. They work at a hospital. They get up at seven o'clock in the morning. They go to the bed at eleven o'clock. +
2. I work in the morning and in the afternoon. I don't work in the evening. I sleep at night. +
3. When do you leave home for school? -- I leave home at a quarter past eight in the morning. +
4. What does your mother do after breakfast? -- She goes to work. +
5. Is there a sofa in your living room? -- Yes, there is a cosy little the sofa in the living room. --Where is the sofa? --It is in the corner of the room to the left of the door. I like to sit on this sofa in front of the TV set in the evening. +
6. There is a nice coffee table near the window. There are newspapers on the coffee table. +
7. There is tea in the cup. +
8. When do you watch TV? --I watch TV in the evening. We have a large colour TV set in our room. There is a beautiful vase on the TV set. There are flowers in the vase. +
9. I have a large writing desk in the study. There is paper on the writing desk. My books and exercice books are on the writing desk, too. +
---

##### Exercise 26
My friend's flat is very comfortable. There are three rooms in the flat: a living room, a study and a bedroom. The living room is not very large. Walls in the living room are blue. There are pictures on the walls. There is a table in the middle of the room with some chairs around it. To the left of the door there is a sofe. Near the sofa there are two large armchairs. They are very comfortable. There is a piano in my friend's living room. The piano is to the right of the door. The bedroom and the study are small. The furnirure in the flat is brown. +
---

##### Exercise 27

1. What colour is your new hat? -- It's red. +
2. Is there a refrigerator in your kitchen? +
3. Where is the refrigerator in your kitchen? -- It is in the corner of the kitchen. +
4. There are flowers in our living room. The flowers are in a beautiful vase. -
5. I have tea in my cup. +
6. He has no coffee in his cup. +
7. What book did you borrow from the library on Tuesday? +
8. I have books, exercice books and pens in my bag. +
9. I am an engineer. I work in an office. I go to the office in the morning. As the office is far from the house I live in, I take a bus to get there. +
10. What bus do you take to get to work? +
11. Whose pen is this? +
12. What colour is your new T-shirt? -- It's white. +
13. She is going to a music shop to by a cassette. -- What cassette is she going to buy? -
14. That's the man who computer was stolen last night. -
15. What books do you like to read? +
16. I don't know what music you are listening to. +

---

##### Exercise 28

Here is a large window. Through the window we can see a room. Opposite the window there is a door. On the door there is a curtain. In the corner of the room there is a round table. On it we can see books and a telephone. There is a bookshelf over the table. On the bookshelf we can see newspapers and a book by Jack London. There are two pictures in the room. One picture is small, the other picture is very large. There are two bookcases in the room. The bookcases are full of books. There is a large sofa in the room. On the sofa we can see a bag. Whose bag is that? There is no bed in the room. On a small table near the window there are flowers and a glass of water. In front of the window we can see an armchair. It is a comfortable armchair. -

---

##### Exercise 29

1. English is a world language. -
2. I study English. I attend English classes in the evening. On the days when I have no classes, I stay at home and do some work about the house. I have dinner with my family. After dinner I talk to the members of my family, watch TV and read books, newspapers and magazines. I go to bed late at night. -
3. I am thirsty. Give me water, please. +
4. There is a book on the table. Give me the book, please. +
5. I enjoy listening to music but I don't really like the music which that man performs on his guitar. +
6. What did you have for lunch? -- I only had a tuna sandwich. +
7. He is a wonderful teacher. He teaches English. +
8. His son is planning to get a degree in computer science. +

---

##### Exercise 30

1. When my grandfather was a young man, he studied physics. +
2. Do you speak Spannish? +
3. My uncle is a great specialist on English literature. -
4. Japanese is more difficult than French. +
5. We listened to a very interesting lecture on English history yesterday. -
6. Yesterday at the lesson of geography the teacher told us very interesting things about famous travellers. +
7. My father speaks English and French, but he doesn't speak German. +
8. We had a lesson of mathematics yesterday. We wrote a paper in mathematics. The teacher said, "I shall correct paper in the evening. Tomorrow you will know the results" +
9. My brother is an expert on geology. He enjoys the beauties of nature is Siberia. +
10. My friend is studying law. +

---

##### Exercise 31

1. Do you play the piano?
2. There is a big black piano in our living room. It is at the wall to the left of the door opposite the sideboard. My mother likes to play the piano. She often plays the piano in the evening.
3. Boys like to play football.
4. What do you do in the evening? -- I often play chess with my grandfather.
5. Where are the children? -- Oh, they are out of the doors. The weather is fine today. They are playing badminton in the yard.
6. What games does you sister like to play? -- She likes to play tennis.
7. Do you like to play the guitar?
8. What color is your guitar?
9. My name is Charlie, I come from a pretty big family. I have two brothers and a sister. My sister plays the violin realy well an wants to be a professional musician. She has other hobbies, too, and she often goes swimming with her friends if weather is nice. After work I like to relax. I play the piano for relaxation. On Sunday my brothers and I play golf if there is a time.

---

##### Exercise 32

It was a hot day. The sun was shining brightly in the blue sky. THe wolf and the lamb met at stream. The water in the stream was cool and clear. The wolf saw that the lamb was fat and wanted to eat it. He began to shout, "You, fool, you are making the water dirty!". The lamb was afraid. It looked at the wolf and said in a thin voice, "But, Mr Wolf, I cannot make the water dirty for you from the place where I am standing, because the stream runs from you to me." "Stop talking!" shouted the wolf in an angry voice. "I know you! I met you six months agom and you were very rude to me." "You are wrong, Mr Wolf," cried the lamb, "you could not have met me six months ago: I am obly four months old." "Never mind," said the wolf, "If it wasn't you, it was your brother." And with these words he seized the poor lamb and carried it into the wood.

---

##### Exercise 33

1. Bill Robbins was a very rich man. He was the richest man in the village.
2. Pete is the tallest boy in our class. Nick is the shortest boy, but he is very strong. He is stronger than many boy who a taller than he. I think Nick is the strongest boy in the class.
3. Granny often tell us long stories. Today her story was still longer. It was the longest story. She began telling it after dinner and finished only before supper. But the story was very interesting. I think it was the most interesting of Granny's stories.
4. Which was the most difficult exercise in the paper?
5. Which is the best season of the year?
6. February is the shortest month of the year.
7. Do you know the longest river in our country?
8. In May days are longer than in April.
9. He is the most intelligent person I know.

---

##### Exercise 34

1. Moscow is situated on the Moscow River. The Moscow is a river that moves very slowly. There is a canal called the Moscow-Volga Canal which joins the Moscow to the Volga. The Volga runs into the Caspian Sea.
2. Several rivers run into the sea at New York. The most important is the Hudson River which empties into the Atlantic Ocean. Besides the Hudson there are two other rivers: the East River and The Harlem river.
3. In Siberia there are many long rivers: the Ob, the Irtysh, the Enisey, the Lena and the Amur.
4. The ALtai Mountains are the higher than the Urals.
5. There is a splendid view of the Lake Geneva from this hotel.
6. My friends have travelled a lot. This year they are going to fly to the Canary Islands.
7. Which river flows through London? -- the Thames.
8. Of which country is Washington capital? -- The United States.
9. The United Kingdom consist of the Great Britain and the Northern Ireland.
10. Chicago is on the Lake Michigan,

---

##### Exercise 35

---

##### Exercise 36

---

##### Exercise 37

---

##### Exercise 38

---

##### Exercise 39

---

##### Exercise 30

---

##### Exercise 41

---

##### Exercise 42

---

##### Exercise 43

---

##### Exercise 44

---

##### Exercise 45

---

##### Exercise 46

---

##### Exercise 47

---

##### Exercise 48

---

##### Exercise 49

---

##### Exercise 50

---

##### Exercise 51

---

##### Exercise 52

---

##### Exercise 53

---

##### Exercise 54

---

##### Exercise 55

---

##### Exercise 56

---

##### Exercise 57

---

##### Exercise 58

---

##### Exercise 59

---

##### Exercise 60

---

##### Exercise 61

---

##### Exercise 62

---

##### Exercise 63

---

##### Exercise 64

---

##### Exercise 65

---

##### Exercise 66

---

##### Exercise 67

---

##### Exercise 68

---

##### Exercise 69

---

##### Exercise 70

---

##### Exercise 71

---

##### Exercise 72

---

##### Exercise 73

---

##### Exercise 74

---

##### Exercise 75

---

##### Exercise 76

---

##### Exercise 77

---

##### Exercise 78

---

##### Exercise 79

---

##### Exercise 80

---

##### Exercise 81

---

##### Exercise 82

---

##### Exercise 83

---

##### Exercise 84

---

##### Exercise 85

---

##### Exercise 86

---

##### Exercise 87

---

##### Exercise 88

---


* "+" -- предложение выполнено верно.
* "-" -- в предложении была допущена и исправлена ошибка
